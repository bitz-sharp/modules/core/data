﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Services;
using Sbatman.Serialize;

namespace Bitz.Modules.Core.Data
{
    public class DataService : Service, IDataService
    {
        private Dictionary<String, Object> _DataStore;
        private IsolatedStorageFile _IsolatedStore;
        private const String DATASTORE_FILENAME = "Data.Store";
        private readonly ILoggerService _LoggerService;

        public DataService()
        {
            _LoggerService = Injector.GetSingleton<ILoggerService>();
        }
        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
        }

        public override void Initialize()
        {
            _DataStore = new Dictionary<String, Object>();

            Load();
        }

        protected override void Shutdown()
        {
            _DataStore.Clear();
            _DataStore = null;
        }

        public T GetValue<T>(String name)
        {
            if (!_DataStore.TryGetValue(name, out Object outObj))
            {
                throw new ArgumentException($"{name} not found by Data Service", nameof(name));
            }
            return (T)outObj;
        }

        public Boolean HasValue(String name)
        {
            return _DataStore.ContainsKey(name);
        }

        public void SetValue<T>(String name, T value)
        {
            if (typeof(T) == typeof(String)
                || typeof(T) == typeof(Guid)
                || typeof(T) == typeof(Packet)
                || typeof(T) == typeof(Double)
                || typeof(T) == typeof(Single)
                || typeof(T) == typeof(Byte[])
                || typeof(T) == typeof(Int16)
                || typeof(T) == typeof(Int32)
                || typeof(T) == typeof(Int64))
            {
                if (_DataStore.ContainsKey(name))
                {
                    _DataStore[name] = value;
                }
                else
                {
                    _DataStore.Add(name, value);
                }
                Save();
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(value), "The type of Value is not supported by the Data Service at this time");
            }

        }

        public override String GetServiceDebugStatus()
        {
            return "";
        }

        private void Save()
        {
            Packet savePacket = new Packet(15000);
            savePacket.Add(_DataStore.Count);
            foreach (KeyValuePair<String, Object> pair in _DataStore)
            {
                savePacket.Add(pair.Key);
                savePacket.AddObject(pair.Value);
            }
            Byte[] data = savePacket.ToByteArray();
            savePacket.Dispose();

            WriteToIsolated(DATASTORE_FILENAME, BitConverter.GetBytes(data.Length), 0);
            WriteToIsolated(DATASTORE_FILENAME, data, sizeof(Int32));
        }

        protected void Load()
        {
            Byte[] fileSizeBytes = ReadFromIsolated(DATASTORE_FILENAME, 0, sizeof(Int32));

            if (fileSizeBytes == null)
            {
                _LoggerService.Log(LogSeverity.WARNING, "Data Service - Load found no data");
                return;
            }
            try
            {
                Int32 fileSize = BitConverter.ToInt32(fileSizeBytes, 0);
                Byte[] fileBytes = ReadFromIsolated(DATASTORE_FILENAME, sizeof(Int32), fileSize);

                Packet dataPacket = Packet.FromByteArray(fileBytes);
                Object[] objects = dataPacket.GetObjects();
                Int32 objectCount = (Int32)objects[0];
                Int32 loc = 0;

                for (Int32 i = 0; i < objectCount; i++)
                {
                    _DataStore.Add((String)objects[loc + 1], objects[loc + 2]);
                    loc += 2;
                }

                dataPacket.Dispose();
            }
            catch (Exception e)
            {
                _LoggerService.Log(LogSeverity.ERROR, "Data Service - Load failed during unpacking phase");
                _LoggerService.Log(LogSeverity.ERROR, e.ToString());
            }
        }

        private void WriteToIsolated(String file, Byte[] data, Int32 position)
        {
            try
            {
                if (_IsolatedStore == null) SetupIsolatedStore();

                System.Diagnostics.Debug.Assert(_IsolatedStore != null, "_IsolatedStore != null");

                using (IsolatedStorageFileStream fs = _IsolatedStore.OpenFile($"Data\\{file}", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    fs.Lock(position, data.Length);

                    fs.Position = position;
                    fs.Write(data, 0, data.Length);

                    fs.Unlock(position, data.Length);

                    fs.Flush();
                    fs.Close();
                }
            }
            catch (Exception e)
            {
                _LoggerService.Log(LogSeverity.ERROR, $"Data Service - Failed to write to isolated storage file {file} at position {position}");
                _LoggerService.Log(LogSeverity.ERROR, e.ToString());
            }
        }

        private Byte[] ReadFromIsolated(String file, Int32 position, Int32 dataLength)
        {
            Byte[] data = new Byte[dataLength];
            try
            {
                if (_IsolatedStore == null) SetupIsolatedStore();

                String targetFile = $"Data\\{file}";

                System.Diagnostics.Debug.Assert(_IsolatedStore != null, "_IsolatedStore != null");

                if (!_IsolatedStore.FileExists(targetFile)) return null;

                using (IsolatedStorageFileStream fileStream = _IsolatedStore.OpenFile(targetFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    fileStream.Lock(position, dataLength);
                    {
                        fileStream.Position = position;
                        fileStream.Read(data, 0, data.Length);
                    }
                    fileStream.Unlock(position, dataLength);
                }
            }
            catch (Exception e)
            {
                _LoggerService.Log(LogSeverity.ERROR, $"Data Service - Failed to read from isolated storage file {file} at position {position}");
                _LoggerService.Log(LogSeverity.ERROR, e.ToString());
            }
            return data;
        }

        private void SetupIsolatedStore()
        {
            _IsolatedStore = IsolatedStorageFile.GetUserStoreForAssembly();
            if (!_IsolatedStore.DirectoryExists("Data")) _IsolatedStore.CreateDirectory("Data");
        }
    }
}